__author__ = 'moritz'

import numpy as np

def directed_vector(direction):
    return direction / np.linalg.norm(direction)

class Edge():
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def split(self, n):
        return [Edge(directed_vector(self.start + i * (self.end - self.start) / n),
                     directed_vector(self.start + (i + 1) * (self.end - self.start) / n)) for i in range(n)]

    def __str__(self):
        return "beam([%s], [%s], 0.01);" % (",".join([str(i) for i in self.start]), ",".join([str(i) for i in self.end]))

    def __repr__(self):
        return "Edge(%r, %r,)" % (self.start, self.end)

    def length(self):
        return np.linalg.norm(self.end - self.start)


class Triangle():
    def __init__(self, corners):
        self.edges = [Edge(corners[i], corners[(i + 1) % len(corners)]) for i in range(len(corners))]

    def split(self, n):
        tmp_edges = []
        new_edges = []
        for e in self.edges:
            n_e = e.split(n)
            tmp_edges.append(n_e)
        for i in range(len(tmp_edges)):
            for j in tmp_edges[i]:
                new_edges.append(j)
            for j in range(1, n):
                for k in Edge(tmp_edges[i][j].start, tmp_edges[(i+1) % len(tmp_edges)][n-j].start).split(n - j):
                    new_edges.append(k)
        return new_edges

if __name__ == "__main__":
    import sys

    n = int(sys.argv[1])

    openscad_template = """$fn=10;

module beam(source, dest, diameter) {
    hull() {
        translate(source) sphere(d=diameter);
        translate(dest) sphere(d=diameter);
    }
}
"""
    print(openscad_template)

#    t = Triangle([directed_vector(np.array([1, -1, 1])),
#                  directed_vector(np.array([1, 1, -1])),
#                  directed_vector(np.array([-1, 1, 1]))])

#    t = Triangle([directed_vector(np.array([1, 0, 1])),
#                  directed_vector(np.array([1, 1, -1])),
#                  directed_vector(np.array([-1, 1, 1]))])

    phi = 0.5 * (1 + 5 ** 0.5)
    t = Triangle([
        directed_vector(np.array([0, phi, 1])),
        directed_vector(np.array([phi, 1, 0])),
        directed_vector(np.array([1, 0, phi])),
    ])
    edges = t.split(n)

    length_table = {}

    print("// Beams for %d times devided object:" % (n,))
    for e in edges:
        print(str(e))
        l = int(e.length() * 1000) / 1000.
        if l not in length_table:
            length_table[l] = 1
        else:
            length_table[l] += 1
    print("\n// Length table for n=%d:" % (n,))
    total_length = 0
    total_number = 0
    max_length = sorted(length_table)[-1]
    for l in sorted(length_table):
        print("//   %2d x %0.2f" % (length_table[l], 50 / max_length * l))
        total_length += length_table[l] * 50 / max_length * l
        total_number += length_table[l]
    print("// total number: %d" % (total_number,))
    print("// total length: %0.2f" % (total_length,))

#    for e in t.edges:
#        print(e)
